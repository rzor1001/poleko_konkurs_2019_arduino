#include <avr/io.h>  
#include <avr/interrupt.h>
#include "Wire.h"

byte lock_motor[3] = {0, 0, 0}; //prev, to_set, enable
byte lock_rotate_counter = 0;
byte display_counter = 0;
int cycle_counter = 0;
int cycle_table[10][2];
int display_digit = 0;
int actual_rotate_speed = 0;
int avg_rotate_table[4] = {0, 0, 0, 0};
byte digits[10] = {0b1000000, 0b1111001, 0b0100100, 0b0110000, 0b0011001, 0b0010010, 0b0000010, 0b1111000, 0b0000000, 0b0010000};
byte second, minute, hour, dayOfWeek, dayOfMonth, month, year;
byte rotate_counter;
byte serial_counter;
byte serial_data[4];
byte disconnect_counter = 0;
byte android_time[6] = {0,0,0,0,0,0}; //hour, minute, show time on display(0-1), blank screen, counter, counter(write to ds1307)
boolean serial_read = false;
boolean cycle_status = false;
boolean rtc_divider = false;
boolean update_time = false;
boolean rtc_reset = false;
boolean reset_timer = false;

#define disp_1 A0
#define disp_2 A1
#define disp_3 A2
#define disp_4 A3
#define display_clock 8
#define display_data 6
#define display_latch 7
#define sensor_pin 2
#define sqw_pin 3
#define relay_left 9
#define relay_right 10
#define button_left 11
#define button_stop 12
#define button_right 13
#define connect_led 5
#define cycle_mode 4
#define DS1307_I2C_ADDRESS 0x68

void setup() {
  pinMode(display_clock, OUTPUT);
  pinMode(display_data, OUTPUT);
  pinMode(display_latch, OUTPUT);
  pinMode(disp_1, OUTPUT);
  pinMode(disp_2, OUTPUT);
  pinMode(disp_3, OUTPUT);
  pinMode(disp_4, OUTPUT);
  pinMode(connect_led, OUTPUT);
  pinMode(relay_left, OUTPUT);
  pinMode(relay_right, OUTPUT);
  pinMode(cycle_mode, OUTPUT);
  pinMode(button_left, INPUT_PULLUP);
  pinMode(button_stop, INPUT_PULLUP);
  pinMode(button_right, INPUT_PULLUP);
  pinMode(sqw_pin, INPUT_PULLUP);
  pinMode(sensor_pin, INPUT_PULLUP);
  
  Serial.begin(9600);
  
  MCUCR|=(1<<ISC01)|(1<<ISC10);
  GICR|=(1<<INT0)|(1<<INT1); 
  TCCR1B|=(1 << CS10)|(1 << WGM12);
  OCR1A=10000;
  SREG|=(1 << 7);
  TIMSK|=(1 << OCIE1A);
  sei();
  //reset_rtc();
  enable_sqw();
  getDateDs1307(&second, &minute, &hour, &dayOfWeek, &dayOfMonth, &month, &year);
  android_time[0] = hour;
  android_time[1] = minute;
}

void loop() {
  while (Serial.available() > 0) {
    byte incoming_byte = Serial.read();
    if (serial_read == false && incoming_byte == 0x01)
    {
      serial_read = true;
      serial_counter = 0;
    }
    
    if (serial_read && serial_counter < 5)
    {
      if (serial_counter != 0)
      {
        serial_data[serial_counter-1] = incoming_byte;
      }
      serial_counter++;
    } 
   
    if (serial_counter == 5)
    {
      if (serial_data[3] == 0x04)
      {
        parse_query(serial_data[0], serial_data[1], serial_data[2]);
      }
      serial_read = false;
      serial_counter=0;
    }  
}
  digitalWrite(cycle_mode, !cycle_status);
  if (disconnect_counter > 6) digitalWrite(connect_led, LOW);
  else digitalWrite(connect_led, HIGH);
  
  if (!digitalRead(button_stop) && !cycle_status){
    rotate_stop(false);
    delay(500);
  }
  
  else if (!digitalRead(button_left) && !cycle_status){
    rotate_left(false);
    delay(500);
  }
  
  else if (!digitalRead(button_right) && !cycle_status){
    rotate_right(false);
    delay(500);
  }
  
  if (lock_motor[2] && actual_rotate_speed == 0)
    {
      if (lock_motor[1] == 1) rotate_left(false);
      else if (lock_motor[1] == 2) rotate_right(false);
      lock_motor[2] = 0;
    }
    
  if (update_time)
    {
        getDateDs1307(&second, &minute, &hour, &dayOfWeek, &dayOfMonth, &month, &year);
        android_time[0] = hour;
        android_time[1] = minute;
        update_time = false;
    }
    
  if (rtc_reset)
 {
   reset_rtc();
   rtc_reset = false;
 }
 
 if (reset_timer){
   android_time[4] = 0;
   reset_timer = true;
 }
  }
  
  boolean rotate_lock(byte rotate_to_set){
    byte tmp_actual_rotate_direction = 0;
    if (digitalRead(relay_left)) tmp_actual_rotate_direction = 1;
    else if (digitalRead(relay_right)) tmp_actual_rotate_direction = 2;
    lock_motor[0] = tmp_actual_rotate_direction;
    lock_motor[1] = rotate_to_set;
    if (lock_motor[0] > 0 && lock_motor[1] > 0 && lock_motor[0] != lock_motor[1] && actual_rotate_speed > 0)
    {
      lock_motor[2] = 1;
      digitalWrite(relay_left, LOW);
      digitalWrite(relay_right, LOW);
      return true;
    }
    else return false;
  }
  
  void rotate_left(boolean is_interrupt){
    android_time[4] = 0;
    lock_rotate_counter = 0;
    if (!rotate_lock(1))
    {
      digitalWrite(relay_right, LOW);
      digitalWrite(relay_left, HIGH);
      //if (!digitalRead(relay_right)) digitalWrite(relay_left, HIGH);
    }
    if (!is_interrupt) reset_rtc();
  }
  
  void rotate_right(boolean is_interrupt){
    android_time[4] = 0;
    lock_rotate_counter = 0;
    if (!rotate_lock(2))
    {
      digitalWrite(relay_left, LOW);
      digitalWrite(relay_right, HIGH);
      //if (!digitalRead(relay_left)) digitalWrite(relay_right, HIGH);
    }
    if (!is_interrupt) reset_rtc();
  }
  
  void rotate_stop(boolean is_interrupt){
    android_time[4] = 0;
    lock_rotate_counter = 0;
    if (!rotate_lock(0))
    {
      digitalWrite(relay_left, LOW);
      digitalWrite(relay_right, LOW);
    }
    if (!is_interrupt) reset_rtc();
}

void reset_rtc()
{
   delay(60);
   setDateDs1307(android_time[1], android_time[0]);
   enable_sqw();
}

void parse_query(byte function, byte data1, byte data2)
{
  disconnect_counter = 0;
  unsigned int data = (data1*256) + data2;
  if (function > 0x03 && function < 0x0a){
    getDateDs1307(&second, &minute, &hour, &dayOfWeek, &dayOfMonth, &month, &year);
  }
  
  if (function == 0x01){ //rotate stop 
    if (data == 1 && !cycle_status){
      rotate_stop(false);
    }
  }
  
  else if (function == 0x02){ //rotate left
    if (data == 1 && !cycle_status){
      rotate_left(false);
    }
  }
  
  else if (function == 0x03){ //rotate right
    if (data == 1 && !cycle_status){
      rotate_right(false);
    }
  }
  
  else if (function >= 0x05 && function <= 0x0e) //set cycle value
  {
    cycle_table[function - 0x05][0] = data;
    send_response(function, data);
  }
  
  else if (function >= 0x0f && function <= 0x18) //set cycle direction
  {
    cycle_table[function - 0x0f][1] = data;
    send_response(function, data);
  }
  
  else if (function == 0x19){ //cycle on
    cycle_counter = 0;
    cycle_status = 1;
    send_response(0x14, cycle_status);
  }
  
  else if (function == 0x1a){ //cycle off
    cycle_status = 0;
    rotate_stop(false);
    send_response(function, cycle_status);
  }
  
  else if (function == 0x1b) //get actual rpm
  {
    send_response(function, actual_rotate_speed);
  }
  
  else if (function == 0x1c) //get actual rotate dir
  {
    if (digitalRead(relay_left)){
      send_response(function, 1);
    }
    
    else if (digitalRead(relay_right)){
      send_response(function, 2);
    }
    
    else {
      send_response(function, 0);
    }
  }
  
  else if (function == 0x1d) //get auto mode
  {
    send_response(function, cycle_status);
  }
  
  else if (function >= 0x1e && function <= 0x27){ //get cycle value
    send_response(function, cycle_table[function-0x1e][0]);
  }
  
  else if (function >= 0x28 && function <= 0x31){ //get cycle direction
    send_response(function, cycle_table[function-0x28][1]);
  }
  
  else if (function == 0x32){ //set hour and minute
    android_time[0] = data / 256;
    android_time[1] = data % 256;
    if (android_time[5] == 0) {
      setDateDs1307(android_time[1], android_time[0]);
      android_time[5]++;
    }
    else if (android_time[5] > 200) android_time[5] = 0;
    else android_time[5]++;
  }
}

void send_response(byte function, unsigned int data)
{
  byte tmp_data[2];
  tmp_data[0] = data / 256;
  tmp_data[1] = data % 256; 
  Serial.write(0x01);
  Serial.write(function);
  Serial.write(tmp_data[0]);
  Serial.write(tmp_data[1]);
  Serial.write(0x04);
}

void enable_sqw()
{
  Wire.beginTransmission(0x68);
  Wire.write(0x07);
  Wire.write(0x10);
  Wire.endTransmission();  
}

byte decToBcd(byte val)
{
  return ( (val/10*16) + (val%10) );
}

byte bcdToDec(byte val)
{
  return ( (val/16*10) + (val%16) );
}

void stopDs1307()
{
  Wire.beginTransmission(DS1307_I2C_ADDRESS);
  Wire.write(0);
  Wire.write(0x80);
  Wire.endTransmission();
}

void setDateDs1307(byte minute, byte hour)
{
   Wire.beginTransmission(DS1307_I2C_ADDRESS);
   Wire.write(0);
   Wire.write(decToBcd(0));
   Wire.write(decToBcd(minute));
   Wire.write(decToBcd(hour));
   Wire.write(decToBcd(1));
   Wire.write(decToBcd(1));
   Wire.write(decToBcd(1));
   Wire.write(decToBcd(1));
   Wire.endTransmission();
}

void getDateDs1307(byte *second, byte *minute, byte *hour, byte *dayOfWeek, byte *dayOfMonth, byte *month, byte *year)
{
  Wire.beginTransmission(DS1307_I2C_ADDRESS);
  Wire.write(0);
  Wire.endTransmission();
  
  Wire.requestFrom(DS1307_I2C_ADDRESS, 7);

  *second     = bcdToDec(Wire.read() & 0x7f);
  *minute     = bcdToDec(Wire.read());
  *hour       = bcdToDec(Wire.read() & 0x3f);
  *dayOfWeek  = bcdToDec(Wire.read());
  *dayOfMonth = bcdToDec(Wire.read());
  *month      = bcdToDec(Wire.read());
  *year       = bcdToDec(Wire.read());
}

void reset_display(){
  digitalWrite(disp_1, HIGH);
  digitalWrite(disp_2, HIGH);
  digitalWrite(disp_3, HIGH);
  digitalWrite(disp_4, HIGH);
}

ISR(TIMER1_COMPA_vect)
{
  reset_display();
  display_counter++;
  if (display_counter == 1)
  {
    if (!android_time[2])
    {
      shift_out(digits[display_digit/1000]<<1);
      if (display_digit >= 1000) digitalWrite(disp_1, LOW);
    }
    else {
      if (android_time[3]){
        shift_out(digits[android_time[0]/10]<<1);
        digitalWrite(disp_1, LOW);
      }
    }
  }
  
  else if (display_counter == 2)
  {
    if (!android_time[2])
    {
      shift_out(digits[(display_digit%1000)/100]<<1);
      if (display_digit >= 100) digitalWrite(disp_2, LOW);
    }
    else {
      if (android_time[3]){
        shift_out(digits[android_time[0]%10]<<1);
        digitalWrite(disp_2, LOW);
      }
    }
  }
  
  else if (display_counter == 3)
  {
    if (!android_time[2])
    {
      shift_out(digits[(display_digit%100)/10]<<1);
      if (display_digit >= 10) digitalWrite(disp_3, LOW);
    }
    else {
      if (android_time[3]){
        shift_out(digits[android_time[1]/10]<<1);
        digitalWrite(disp_3, LOW);
      }
    }
  }
  
  else if (display_counter == 4)
  {
    if (!android_time[2])
    {
      shift_out(digits[display_digit%10]<<1);
      digitalWrite(disp_4, LOW);
    }
    else {
      if (android_time[3]){
        shift_out(digits[android_time[1]%10]<<1);
        digitalWrite(disp_4, LOW);
      }
    }
    display_counter=0;
  }
}

void shift_out(byte data){
  digitalWrite(display_latch, LOW);
  shiftOut(display_data, display_clock,MSBFIRST,data);
  digitalWrite(display_latch, HIGH);
}

int avg_rotate(int number_of_turns){
  int avg_rotate_table_copy[4];
  for(int x=0; x<4; x++){
    avg_rotate_table_copy[x] = avg_rotate_table[x];
  }
  for(int x=0; x<3; x++){
    avg_rotate_table[x] = avg_rotate_table_copy[x+1];
  }
  avg_rotate_table[3] = number_of_turns;
  int avg = 0;
  for(int x=0; x<4; x++){
    avg = avg + avg_rotate_table[x];
  }
  return int(float(avg)/4.0);
}

ISR (INT0_vect)
{
    rotate_counter++;
    //if (!reset_timer) reset_timer = true;
    //android_time[4] = 0;
    //if (rotate_counter > 100) {
    //  setDateDs1307(android_time[1], android_time[0]);
    //  enable_sqw();
    //}
}

ISR (INT1_vect)
{
    if (actual_rotate_speed) android_time[4] = 0;
    if (lock_rotate_counter < 2) lock_rotate_counter++;
    else 
    {
      byte add_multiple = 1;
      if (cycle_status) add_multiple = 2;
      for (int i=0; i < add_multiple; i++) 
      actual_rotate_speed = avg_rotate(int(float(rotate_counter/2.0) * 120.0));
    }
    display_digit = actual_rotate_speed;
    rotate_counter = 0;
    disconnect_counter++;
    if (disconnect_counter > 250) disconnect_counter = 10;
    
    rtc_divider = !rtc_divider;
    if (rtc_divider && cycle_status && !lock_motor[2]){
      unsigned int sum = 0;
      for (int x=0; x<10; x++)
      {
        if (x != 0) sum = (sum + cycle_table[x][0]);
        if (cycle_counter == sum){
          if (cycle_table[x][1] == 1){
            rotate_left(true);
           }
          else if (cycle_table[x][1] == 2){
            rotate_right(true);
          }
          else if (cycle_table[x][1] == 0){
            rotate_stop(true);
          }
          rtc_reset = true;
        }
      }
      if (sum < cycle_counter)
      {
        cycle_counter=0;
      }
      cycle_counter++;
    }
    
    if (android_time[4] <= 20){
      android_time[4]++;
      android_time[2] = 0;
    }
    else {
      android_time[2] = 1;
      if (rtc_divider){
        android_time[3] = !android_time[3];
        if (!digitalRead(connect_led)) update_time = true;      
      }
    }
}
  
